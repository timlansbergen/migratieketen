package nl.ind.veroordeling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VeroordelingApplication {

    public static void main(String[] args) {
        SpringApplication.run(VeroordelingApplication.class, args);
    }

}
